JAWABAN TUGAS MATERI 10


Soal 1. Buat Database.

Syntax:
create Database nama_database;


Soal 2. Membuat Tabel (Users, Categories & Items) dalam Database.

Syntax:
create table users(
    -> id int(8) auto_increment primary key,
    -> name varchar(255),
    -> email varchar(255),
    -> password varchar(255)
    -> );

create table categories(
    -> id int(8) auto_increment primary key,
    -> name varchar(255)
    -> );

create table items(
    -> id int(8) auto_increment primary key,
    -> name varchar(255),
    -> description varchar(255),
    -> price int(20),
    -> stock int(10),
    -> category_id int(8),
    -> foreign key(category_id) references categories(id)
    -> );


Soal 3. Memasukkan Data pada Table (Users, Categories & Items)

Syntax:
insert into users(name, email, password) values("John Doe", "john@doe.com", "john123"), ("jane Doe", "jane@doe.com", "jenita123");

insert into categories(name) values("gadget", ("cloth"), ("men"), ("women"), ("branded");

insert into items(name, description, price, stock, category_id) values("Sumsang b50", "hape keren dari merek sumsang", 4000000, 100, 3), ("Uniklooh", "baju keren dari brand ternama", 500000, 50, 4), ("IMHO Watch", "jam tangan anak yang jujur banget", 2000000, 10, 3);


Soal 4. Mengambil Data dari Database

Syntax a:
select id, name, email from users;

Syntax b:
select * from items where name like '%watch';

Syntax c:
select items.name, items.description, items.price, items.stock, items.category_id, categories.name from items inner join categories on items.category_id = categories.id;


Soal 5. Mengubah Data dari Database

Syntax:
update items set price=2500000 where id=1;